import { knex } from "./util/db";
import dotenv from 'dotenv';
import { Request, Response, NextFunction } from "express";
dotenv.config();
import grant from 'grant';
import express from "express";
import UserService from "./service/user_service";
import UserController from "./controller/user_controller";
import env from "./util/env";
import http from "http";
import { Server as SocketIO } from "socket.io";
import { setSocketIO } from "./util/socketIO";
import { RouterOptions } from "./models";
import { createRouter } from "./routes/router";
import { isLoggedIn } from "./middlewares/guard";
import {
    expressSessionGate,
    firstLandingPage,
    grantExpress,
    handle404,
    requestLogger
} from "./middlewares/app-middleware";
import path from "path";
import ClothesSerivce from "./service/clothes_serivce";
import ClothesController from "./controller/clothes_controller";
import expressSession from "express-session";
import { Knex } from "knex";
import { resourceLimits } from "worker_threads";

const app = express();

export const server = new http.Server(app);
export const io = new SocketIO(server);
const userService = new UserService(knex)
const userController = new UserController(userService, io)
const clothesService = new ClothesSerivce(knex)
const clothesController = new ClothesController(clothesService)

app.use(express.json());
app.use(expressSessionGate);
app.use(grantExpress as express.RequestHandler);
app.use(requestLogger);

setSocketIO(io);


let routerOptions: RouterOptions = {
    app,
    userController,
    clothesController
};

createRouter(routerOptions);

//for temporary use
app.post("/get_ootd", isLoggedIn, async (req: Request, res: Response) => {
    if (isNaN(req.body.content)) {
        res.json({ "type": "invalid input" })
    } else {
        let temperature = req.body.content
        let user = req.session["user"];
        let userID = user.id
        let userclothes = await knex('clothes').select(`*`).where('user_id', userID)
        let upperclothes = []
        let lowerclothes = []
        let wholebody = []
        let result = []
        if (parseInt(temperature) <= 18) {
            for (let x of userclothes) {
                if (x.main_type == "top" && (x.season == "winter" || x.season == "autumn")) {
                    upperclothes.push(x)
                } else if (x.main_type == "bottom" && (x.season == "winter" || x.season == "autumn")) {
                    lowerclothes.push(x)
                } else if (x.main_type == "full" && (x.season == "winter" || x.season == "autumn")) {
                    wholebody.push(x)
                }
            }
        } else {
            for (let x of userclothes) {
                if (x.main_type == "top" && (x.season == "summer" || x.season == "spring")) {
                    upperclothes.push(x)
                } else if (x.main_type == "bottom" && (x.season == "summer" || x.season == "spring")) {
                    lowerclothes.push(x)
                } else if (x.main_type == "full" && (x.season == "summer" || x.season == "spring")) {
                    wholebody.push(x)
                }
            }
        }
        if (wholebody.length + upperclothes.length < 2 || lowerclothes.length < 2) {
            res.json({"type": "limited_clothes" })
        } else {
            var probability_whole = wholebody.length / (upperclothes.length + wholebody.length)
            if (Math.random() + 0.000001 <= probability_whole) {
                var num = Math.floor(Math.random() * wholebody.length)
                result.push({ "path1": wholebody[num].processed_image, "path2": null, "type": "full" })
                wholebody.splice(num, 1)
            } else {
                var num = Math.floor(Math.random() * upperclothes.length)
                var num2 = Math.floor(Math.random() * lowerclothes.length)
                result.push({ "path1": upperclothes[num].processed_image, "path2": lowerclothes[num2].processed_image, "type": "not_full" })
                upperclothes.splice(num, 1)
                lowerclothes.splice(num2, 1)
            }
            var probability_whole = wholebody.length / (upperclothes.length + wholebody.length)
            if (Math.random() + 0.000001 <= probability_whole) {
                var num = Math.floor(Math.random() * wholebody.length)
                result.push({ "path1": wholebody[num].processed_image, "path2": null, "type": "full" })
                wholebody.splice(num, 1)
            } else {
                var num = Math.floor(Math.random() * upperclothes.length)
                var num2 = Math.floor(Math.random() * lowerclothes.length)
                result.push({ "path1": upperclothes[num].processed_image, "path2": lowerclothes[num2].processed_image, "type": "not_full" })
                upperclothes.splice(num, 1)
                lowerclothes.splice(num2, 1)
            }

            res.json(result)
        }
    }
})

//default landing page --> login page
app.get("/", firstLandingPage)
app.use(express.static("public"));
app.use(express.static("public/user_icon"));
app.use(express.static("public/login"));
app.use(express.static("uploads"))
app.use("/login_success", isLoggedIn, express.static("private"));
app.use((req: express.Request, res: express.Response) => {
    res.sendFile(path.resolve("./public", "404.html"));
});


server.listen(env.SERVER_PORT, () => {
    console.log(`Listening at http://localhost:${env.SERVER_PORT}/`);
});



