import UserService from "../service/user_service";
import {Request, Response, NextFunction} from "express";
import {Server as SocketIO} from "socket.io";
import path from "path";
import multer from "multer";

export default class UserController {
    private userService: UserService;
    private io: SocketIO;

    constructor(userService: UserService, io: SocketIO) {
        this.userService = userService;
        this.io = io;
    }

    login = async (req: Request, res: Response) => {
        try {
            const {email, password} = req.body
            if (!email || !password) {
                res.status(400).json({msg: "missing login data"})
                return
            }
            res.json(await this.userService.checkLogin(email, password, req))
        } catch (error) {
            console.log(error);
        }
    }

    googleLogin = async (req: Request, res: Response) => {
        try {
            const accessToken = req.session?.['grant'].response.access_token;
            const fetchRes = await this.userService.fetchGoogleInfo(accessToken)
            const result = await fetchRes.json();
            const user = await this.userService.checkGoogleLogin(result)
            if (req.session) {
                req.session['user'] = user;
            }
            return res.redirect('/login_success/wardrobe.html')
        } catch (error) {
            console.log(error);

        }
    }


    userIcon = async (req: Request, res: Response) => {
        try {
            console.log("req.file: ", req.file);
            if(!req.file || !req.file.filename){
                res.json('fileName missing')
                return 
            }
            const  {id: userID} = req.session["user"];
            const fileName  = req.file.filename
            res.json(await this.userService.updateUserIcon(fileName, userID, req))
        } catch (error) {
            console.log(error);
        }
    }
    getProfile = async(req: Request, res: Response) => {
        res.json(req.session["user"])
    }
}
