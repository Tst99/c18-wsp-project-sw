import ClothesSerivce from "../service/clothes_serivce";
import {Request, Response} from 'express'
import { clothesStyleOption, Msg } from "../models";
import path from "path";
import { createJsonObj } from "../util/help";



export default class ClothesController{
    constructor(private clothesService: ClothesSerivce){}
    getClothes = async (req:Request, res: Response)=>{
        const styleOption:clothesStyleOption = req.body
        const {id} = req.session['user']
        const clothesResult = await this.clothesService.getClothes(styleOption, id)
        res.json({clothesResult})
    }
    getStyleOption = async(req:Request, res: Response)=>{
        const {id} = req.session['user']
        res.json(await this.clothesService.getStyleOption(id))
    }
    likeClothes = async(req:Request, res: Response)=>{
       const {clothesId, main_type} = req.params
       const {id} = req.session['user']
       if(!parseInt(clothesId) || !main_type){
           res.status(400).json({msg:'invalid parameter'})
           return 
       }
       const result:Msg = await this.clothesService.likeClothes(id, parseInt(clothesId), main_type)
       res.json(result)
    }
    unlikeClothes = async(req: Request, res: Response)=>{
        const {clothesId, main_type} = req.params
        console.log(parseInt(clothesId));
        if(!parseInt(clothesId) || !main_type){
            res.json({msg: 'invalid input params'})
            return 
        }
        res.json(await this.clothesService.unLikeClothes(parseInt(clothesId), main_type))
    }
    deleteClothes = async(req:Request, res: Response)=>{
        const {clothesId, main_type} = req.params
        if(!parseInt(clothesId) || !main_type){
            res.json({msg: 'invalid input params'})
            return 
        }
        res.json(await this.clothesService.deleteClothes(parseInt(clothesId), main_type))
    }

    uploadBeforeImage = async (req:Request, res: Response)=>{
        if(!req.file){
            res.status(400).json({msg: 'No files is found'})
            return 
        }
        const fileName = req.file.filename
        const beforeFileRoute = `/uploads/beforeClothes/${fileName}`
        req.session['uploadClothes'] = {beforeFileName: fileName, beforeFileRoute}
        res.json({msg: 'upload image successful'})
    }
    processUploadImage = async (req:Request, res: Response)=>{
        console.log('process the image!!!!!!!');
        const filePath = await this.clothesService.RemoveBackground(req)
        const {msg, path} = filePath
        if(msg || !path){
            res.json(msg)
            return
        }        
        res.sendFile(path)
        // const AfterImageAbsPath = path.resolve(__dirname, filePath)
        // const resRemoved = await this.clothesService.RemoveBackground(fileName)
        // const fileAbsPath = path.resolve(req.file.destination, req.file.filename)
        // res.sendFile(AfterImageAbsPath)
    }

    getLikeButtonInfo = async(req:Request, res: Response)=>{
        const {id} = req.session['user']
        res.json(await this.clothesService.getLikeButtonInfo(id))
    }
    HeicToPng = async(req:Request, res: Response)=>{
        try {
            if(!req.file){
                res.json({msg: 'invalid HEIC image'})
                return
            }
            const filePath = path.join(req.file.destination, req.file.filename)
            const outputBuffer = await this.clothesService.convertPNG(filePath)
            // const inputBuffer = await promisify(fs.readFile)(path.join(req.file.destination, req.file.filename))
            // const outputBuffer = await convert({
            //     buffer: inputBuffer, // the HEIC file buffer
            //     format: 'PNG'        // output format
            //   });
            res.end(outputBuffer)
            // await promisify(fs.writeFile)(path.resolve(__dirname, `../private/uploads/beforeClothes/${req.file.filename.split('.heic')[0]}.png`), outputBuffer);
            // req.session['BeforeIMG'] = {fileName: `${req.file.filename.split('.heic')[0]}.png`}
            // res.sendFile(path.resolve(__dirname, `../private/uploads/beforeClothes/${req.file.filename.split('.heic')[0]}.png`))
        } catch (error) {
            console.log(error);
            res.json({msg:'convert HEIC error'})
            return 
        }
    }
    getClothImage = (req: Request, res: Response)=>{
        const fileRoute = req.session['uploadClothes'].afterFileRoute
        if(!fileRoute){
            res.json(createJsonObj(null, 'You have\'t upload an image'))
            return
        }
        res.json(createJsonObj(fileRoute))
    }
    addCloth = async (req: Request, res: Response)=>{
        res.json(await this.clothesService.addCloth(req))
    }
}