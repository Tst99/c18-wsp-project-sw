import { Knex } from "knex";
import { clothesStyleOption, UploadFile } from "../models";
import { promisify } from 'util'
import fs from 'fs'
import path from "path";
import express from 'express'
import fetch from 'node-fetch'
import { createJsonObj } from "../util/help";
const convert = require('heic-convert');


export default class ClothesSerivce{
    private mainTypeMapping
    constructor(private knex: Knex){
        this.mainTypeMapping = {
            'bottom' : 'cloth_lower_id',
            'top' : 'cloth_up_id',
            'full' : 'cloth_full_id'
        }
    }

    getClothes = async (styleOption: clothesStyleOption, userID: number)=>{
        const isLikeSorting = styleOption['like'] 
        let sqlCommand = `select *, clothes.id as clothes_id from clothes `
        //check if inner-join is needed
        if(isLikeSorting) sqlCommand += `join user_emotion  on  clothes.id in (user_emotion.cloth_up_id, user_emotion.cloth_lower_id, user_emotion.cloth_full_id) `
        sqlCommand += `where ${isLikeSorting? 'clothes.':''}user_id =${userID}`
        for (let styleKey in styleOption){
            let index = 1
            const styleList = styleOption[styleKey]

            for(let style of styleList){
                sqlCommand += ` ${index==1?'AND (':'OR'} ${styleKey} = '${style.trimStart()}'`
                index ++
            }
            sqlCommand += styleList.length? ')' : ''
        }
        if(isLikeSorting) sqlCommand += ` AND (user_emotion.emotion_id = ${await this.getLikeId()})`
        console.log(sqlCommand);
        let result  = await this.knex.raw(sqlCommand);
        result  = result.rows
        return result
    }
    getStyleOption = async (userID: number)=>{
        // console.log(userID);
        const res = await this.knex('clothes').select(`*`).where('user_id', userID)
        // const styleOption:clothesStyleOption = {}
        const styleOption:clothesStyleOption = {}
        for(const data of res){
            // const {color, season, brand, main_type, type} = data 
            for(const dataCol in data){
                const styleKey = dataCol
                const styleValue = data[dataCol]

              if(!styleOption[styleKey]){
                  styleOption[styleKey] = []
              }
              if(!styleOption[styleKey].includes(styleValue)){
                styleOption[styleKey].push(styleValue)
              }
            }
        }
        delete styleOption['updated_at']
        delete styleOption['created_at']
        delete styleOption['id']
        delete styleOption['original_image']
        delete styleOption['processed_image']
        delete styleOption['user_id']
        return styleOption
    }
    likeClothes = async (user_id: number, clothesId:number, main_type:string)=>{
        const likeId = await this.getLikeId()
        let found
        const clothIdName = this.mainTypeMapping[main_type]
        if(!clothIdName) return {msg:"mainType Error"}
        found = await this.knex('user_emotion').select('*').where('emotion_id', likeId).and.where(clothIdName, clothesId)
        if(found.length) return {msg:"You've liked this item"}        
        const objInsert = {            
            user_id, 
            'emotion_id':likeId
        }
        objInsert[clothIdName] = clothesId
        await this.knex('user_emotion').insert(objInsert)
        return {msg: "update successful"}
    }

    getLikeButtonInfo = async (userID: number)=>{
        const sql = `select clothes.id from clothes `
                    +`join user_emotion ` 
                    +`on clothes.id in (user_emotion.cloth_up_id, user_emotion.cloth_lower_id, user_emotion.cloth_full_id) `
                    +`join emotion_reference `
                    +`on emotion_reference.id = user_emotion.emotion_id `
                    +`where clothes.user_id  = ${userID} and emotion_reference.emotion = 'like'`
        console.log(sql);
        const Found = (await this.knex.raw(sql)).rows
        console.log('Found', Found);
        if(!Found.length) return []
        const res = Found.map((row: any)=>{
            return row.id
        })
        return res
    }
    unLikeClothes = async (clothesId: number, main_type: string)=>{
        try {
            const mainTypeName = this.mainTypeMapping[main_type]
            await this.knex('user_emotion').where(`${mainTypeName}`, clothesId).del()
            return {msg: 'update successful'}
        } catch (error) {
            console.log(error);
            return {msg: 'psql error'}
        }

    }
    deleteClothes = async (clothesId: number, main_type: string)=>{
        try {
            console.log('deleteTimes');
            
            const clothIdName = this.mainTypeMapping[main_type]
            console.log(clothIdName);
            console.log(clothesId);
            await this.knex('user_emotion').where(clothIdName, clothesId).del()
            await this.knex('clothes').where('id', clothesId).del()
            return {msg: 'delete successful'}
        } catch (error) {
            console.log(error);
            return {msg: 'psql error'}
        }

    }
    convertPNG = async (filePath: string)=>{
        const inputBuffer = await promisify(fs.readFile)(filePath)
        const outputBuffer = await convert({
            buffer: inputBuffer, // the HEIC file buffer
            format: 'PNG'        // output format
          });
        return outputBuffer
    }
    RemoveBackground = async(req:express.Request) =>{
        // console.log(uploadFilePath);
        // console.log(path.join('beforeClothes',uploadFilePath));
        // return path.join('../private/uploads/beforeClothes',uploadFilePath)
        try {
            const {email} = req.session['user']
            const {beforeFileName} = req.session['uploadClothes']
            const absPathUpload = `private/uploads/beforeClothes/${beforeFileName}`
            const savePath = `private/uploads/afterClothes/`
            console.log('upload', absPathUpload);
            console.log('save', savePath);
    
            let result = await fetch("http://localhost:8000/get_mask", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "email": email,
                    "upload_file_path": absPathUpload,
                    "saved_cloth_path": savePath
                })
            })
            const ImagePathRes = await result.json()
            if(!ImagePathRes){
                return {path:null,  msg: 'python server error: No path return'}
            }
            const {Path, afterFileName} = ImagePathRes
            req.session['uploadClothes'].afterFileName = afterFileName
            req.session['uploadClothes'].afterFileRoute = `/login_success/uploads/afterClothes/${afterFileName}`
            return {path: path.resolve(__dirname, Path), msg: null}
        } catch (error) {
            console.log(error);
            return {path:null, msg: 'removeBackground error'}
        }
    }


    private async getLikeId(): Promise<number>{
        const likeIdRes = await this.knex('emotion_reference') .select('id').where('emotion', 'like')
        const likeId = likeIdRes[0].id
        // const disLikeIdRes = await this.knex('emotion_reference') .select('id').where('emotion', 'dislike')
        // const disLikeId = disLikeIdRes[0].id
        return likeId
    }
    // addCloth = async(styleSelection:clothesStyleOption, imageInfo: UploadFile, user_id: number)=>{
    addCloth = async(req:express.Request)=>{
        try {
            const {id: user_id} = req.session['user']
            const {main_type, brand, season, color, editRoute} = req.body
            if(!main_type || !brand || !season|| !color){
                return createJsonObj(null, 'data is missing')
            }
            console.log('editRoute', editRoute)
            if(editRoute != 'null'){
                //To-do
                console.log('editRoute', editRoute)
                await this.knex('clothes').where('processed_image', editRoute).update({
                    main_type, brand, season, color
                })
                return createJsonObj('edit success')
            }
            const {afterFileRoute: processed_image, beforeFileRoute: original_image} = req.session['uploadClothes']
            await this.knex('clothes').insert({
                main_type, brand, season, color, processed_image, original_image, user_id
            })
            req.session['uploadClothes'] = {
                afterFileName: null,
                afterFileRoute: null,
                beforeFileName: null,
                beforeFileRoute: null
            }
            return createJsonObj('save successful')
        } catch (error) {
            console.log(error);
            return createJsonObj(null, 'knex upload fail')
        }
    }
}