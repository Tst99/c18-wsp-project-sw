import { Knex } from "knex";
import { checkPassword, hashPassword } from "../util/hash";
import fetch from 'node-fetch';
import { UserInfo } from "../models";
import express from 'express'

class UserService {
    constructor(private knex: Knex) {}
    checkLogin = async (email: string, password: string, req:Express.Request)=>{
        //checking for username 
        const userInfo:UserInfo[] = await this.knex('users').select('*').where('email', email)
        if(!userInfo[0]){
            return {msg: "Invalid input"}
        }
        //check for password
        if(! await checkPassword(password, userInfo[0].password)){
            return {msg: "Invalid input"}
        }
        req.session['user'] =  userInfo[0]
        req.session['uploadClothes'] = {
            afterFileName: null,
            afterFileRoute: null,
            beforeFileName: null,
            beforeFileRoute: null
        }
        delete req.session['user'].password
        return {msg: "login Success"}
    }
    fetchGoogleInfo = async (accessToken:string)=>{
        const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
            method: "get",
            headers: {
                "Authorization": `Bearer ${accessToken}`,
            },
        });
        return fetchRes
    }
    checkGoogleLogin = async (result:any)=>{
        let users = await this.knex.select("*").from("users").where("email", result.email)
        let user = users?.[0];
        if (!user) {
            users = await this.knex('users').insert(
                {
                    username: result.name,
                    password: await hashPassword('user1'),
                    email: result.email,
                    icon: 'assets/images/defaultIcon.jpg',
                    role: 'user'
                }
            ).returning("*")
            user = users?.[0]
        }
        return user
    }

    updateUserIcon = async (filename:string, userId:string, req:express.Request)=>{
        try {
            const user = (await this.knex('users').update('icon',filename).where('id',userId).returning('*'))[0]

            req.session['user'] = user
            delete req.session['user'].password
            return {msg: 'update icon success'}
        } catch (error) {
            console.log(error);
            return {msg: 'knex update icon fail'}
        }
    }      

}

export default UserService