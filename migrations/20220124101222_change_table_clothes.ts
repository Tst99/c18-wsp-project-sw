import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('clothes', async function(table) {
        table.dropChecks(["clothes_main_type_1"]);
      });
    await knex.schema.alterTable('clothes', async function(table) {
        table.string('main_type', 50).checkIn(['bottom', 'top', 'full']).alter()
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('clothes', async function(table) {
        table.dropChecks(["clothes_main_type_1"]);
      });
    await knex.schema.alterTable('clothes', function(table) {
        table.string('main_type', 50).checkIn(['lower', 'upper', 'full']).alter()
      });
}

