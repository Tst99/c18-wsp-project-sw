import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('clothes', function(table) {
        table.string('original_image', 200).notNullable().alter();
        table.string('processed_image', 200).notNullable().alter();
      });
      
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('clothes', function(table) {
        table.string('original_image', 100).notNullable().alter();
        table.string('processed_image', 100).notNullable().alter();
      });
}

