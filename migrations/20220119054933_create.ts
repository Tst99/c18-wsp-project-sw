import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    // create users table
    let hasTable = await knex.schema.hasTable('users');
    if(!hasTable){
        await knex.schema.createTable('users',(table)=>{
            table.increments('id').primary();
            table.string('username', 50).notNullable();
            table.text('password').notNullable();
            table.string('email', 50).notNullable().unique();
            table.string('icon', 50);
            table.string('role', 10).defaultTo('user');
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
        })
    }
    // create clothes table
    hasTable = await knex.schema.hasTable('clothes');
    if(!hasTable){
        await knex.schema.createTable('clothes',(table)=>{
            table.increments('id').primary();
            table.string('original_image', 100).notNullable();
            table.string('processed_image', 100).notNullable();
            table.integer('user_id').notNullable();
            table.foreign('user_id').references('users.id')
            table.string('color', 50)
            table.string('season', 10).checkIn(['winter', 'summer', 'spring', 'autumn'])
            table.string('brand', 50)
            table.string('main_type', 50).checkIn(['lower', 'upper', 'full'], 'clothes_main_type_1')
            table.string('type', 50)
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
        })
    }
    // create information table
    hasTable = await knex.schema.hasTable('information');
    if(!hasTable){
        await knex.schema.createTable('information',(table)=>{
            table.increments('id').primary();
            table.text('content')
            table.text('url')
            table.integer('user_id')
            table.foreign('user_id').references('users.id')
            table.timestamp('created_at').defaultTo(knex.fn.now());
        })
    }
    // create emotion_reference table
    hasTable = await knex.schema.hasTable('emotion_reference');
    if(!hasTable){
        await knex.schema.createTable('emotion_reference',(table)=>{
            table.increments('id').primary();
            table.string('emotion', 50).notNullable()
        })
    }
    // create user_emotion table
    hasTable = await knex.schema.hasTable('user_emotion');
    if(!hasTable){
        await knex.schema.createTable('user_emotion',(table)=>{
            table.increments('id').primary()
            table.integer('user_id')
            table.foreign('user_id').references('users.id')
            table.integer('cloth_up_id')
            table.integer('cloth_lower_id')
            table.foreign('cloth_up_id').references('clothes.id')
            table.foreign('cloth_lower_id').references('clothes.id')
            table.integer('emotion_id').notNullable()
            table.foreign('emotion_id').references('emotion_reference.id')
            table.timestamp('created_at').defaultTo(knex.fn.now());
            table.timestamp('updated_at').defaultTo(knex.fn.now());
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('user_emotion')
    await knex.schema.dropTableIfExists('emotion_reference')
    await knex.schema.dropTableIfExists('information')
    await knex.schema.dropTableIfExists('clothes')
    await knex.schema.dropTableIfExists('users')
}

