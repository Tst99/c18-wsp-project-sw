import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('user_emotion', async function(table) {
        table.integer('cloth_full_id')
        table.foreign('cloth_full_id').references('clothes.id')
    });
}



export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('user_emotion', async function(table) {
        table.dropColumn('cloth_full_id')
    });
}

