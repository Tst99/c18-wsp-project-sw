import ClothesController from "../controller/clothes_controller";
import express from 'express'
import { isLoggedIn } from "../middlewares/guard";
import { heicImageUpload, uploadBefore } from "../multer/uploadPicture_multer";





export default function createClothesRoute(clothesController: ClothesController){
    const clothesRoute = express.Router()
    try {
        //wardrobe page
        clothesRoute.post('/clothes', isLoggedIn,clothesController.getClothes)
        clothesRoute.get('/styleOption', isLoggedIn,clothesController.getStyleOption)
        clothesRoute.get('/like_button_info', isLoggedIn, clothesController.getLikeButtonInfo)
        clothesRoute.post('/liked-clothes/:clothesId/:main_type', isLoggedIn,clothesController.likeClothes)
        clothesRoute.post('/unliked-clothes/:clothesId/:main_type', isLoggedIn, clothesController.unlikeClothes)
        clothesRoute.delete('/clothes/:clothesId/:main_type', isLoggedIn,clothesController.deleteClothes)
        //upload clothes page
        clothesRoute.post('/before-clothes-image', isLoggedIn, uploadBefore.single("filename"), clothesController.uploadBeforeImage)
        clothesRoute.post('/HEICImage', isLoggedIn, heicImageUpload.single("filename"), clothesController.HeicToPng)
        clothesRoute.get('/processdImage', isLoggedIn, clothesController.processUploadImage)
        //clothe information page
        clothesRoute.get('/clothImage', isLoggedIn, clothesController.getClothImage)
        clothesRoute.post('/new-clothe', isLoggedIn, uploadBefore.single("style_select"),clothesController.addCloth)


    } catch (error) {
        console.log(error);
    }
    return clothesRoute
}


