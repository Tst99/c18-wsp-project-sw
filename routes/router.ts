import { RouterOptions } from "../models";
import createClothesRoute from "./clothes_route";
import { createUserRoutes } from "./user_route";

export function createRouter(options: RouterOptions) {
        const { app, userController, clothesController} = options;
        app.use("/", createUserRoutes(userController));
        app.use("/", createClothesRoute(clothesController));
}
