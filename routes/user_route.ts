
import express from "express";
import UserController from "../controller/user_controller";
import { userIconUpload } from "../multer/user_icon_multer";
// import { mainController } from "../controller/user_controller";



export function createUserRoutes(userController: UserController) {
    const userRoutes = express.Router();
    //To-do 
    //user login, user logout, create new user account, google login, upload icon
    userRoutes.post("/login", userController.login)
    userRoutes.get("/login/google", userController.googleLogin)
    userRoutes.get("/profile", userController.getProfile)
    userRoutes.put("/user/icon", userIconUpload.single("newIcon"), userController.userIcon)
    userRoutes.post("/logout")
    userRoutes.post("/new_account")
   
    return userRoutes;
}
