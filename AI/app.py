import datetime
from sanic import Sanic
from sanic.response import json
import tensorflow as tf
import numpy as np
import cv2
import torch
# from pylab import imshow
import albumentations as albu
from iglovikov_helper_functions.utils.image_utils import pad, unpad
from iglovikov_helper_functions.dl.pytorch.utils import tensor_from_rgb_image
from cloths_segmentation.pre_trained_models import create_model
model = create_model("Unet_2020-10-30")
model.eval()

def predict(before_url, mask_url, after_url):
     # load data
     image = tf.keras.utils.load_img(
          before_url)
     image = tf.keras.utils.img_to_array(image)
     # prediction
     transform = albu.Compose([albu.Normalize(p=1)], p=1)
     padded_image, pads = pad(image, factor=32, border=cv2.BORDER_CONSTANT)

     x = transform(image=padded_image)["image"]
     x = torch.unsqueeze(tensor_from_rgb_image(x), 0)

     with torch.no_grad():
          prediction = model(x)[0][0]

     mask = (prediction > 0).cpu().numpy().astype(np.uint8)
     mask = unpad(mask, pads)
     mask = tf.expand_dims(mask, -1)
     # convert result to image and save it
     pil_img = tf.keras.preprocessing.image.array_to_img(mask)
     tf.keras.utils.save_img(mask_url, mask)
     
     # convert mask to final result
     image1 = tf.keras.utils.load_img(
     before_url, color_mode='rgba')
     image1 = tf.keras.utils.img_to_array(image1)
     mask = tf.squeeze(mask)
     image1[:, :, -1] = image1[:, :, -1] * mask
     pil_img = tf.keras.preprocessing.image.array_to_img(image1)
     tf.keras.utils.save_img(after_url, pil_img)

app = Sanic("AI")

@app.post("/get_mask")
async def callModel(request):
    print(f'someone is connected')
    newtime = datetime.datetime.now()
    time = newtime.strftime("%d%m%Y%H%M%S")
    content = request.json
    email = content["email"]
    upload_file_path = content["upload_file_path"]
    saved_cloth_path = content["saved_cloth_path"]
    savePath = f'../{saved_cloth_path}/After-{email}-{time}.png'
    predict(f"../{upload_file_path}", f'./mask/{email}.jpeg', savePath)
    afterFileName = f'After-{email}-{time}.png'
    # content = request.arraybuffer
    # print(content)
    return json({'Path': savePath, 'afterFileName': afterFileName})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)