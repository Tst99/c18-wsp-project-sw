echo 'creating conda environment...\n'
conda create -p ./venv -y
conda activate ./venv

echo 'installing packages...\n'
conda install -c apple tensorflow-deps -y
pip install tensorflow-macos
conda install matplotlib -y
conda install pandas -y
conda install jupyterlab -y
conda install ipykernel -y
pip install sanic
echo 'Enjoy tensorflow!'
conda activate ./venv

#created by alvin