from fileinput import filename
from glob import glob
from importlib.resources import path
from sanic import Sanic
from sanic.response import json
import numpy as np
import pathlib
import os
# import datetime
# import IPython.display as display
# import matplotlib.pyplot as plt
# import numpy as np
import tensorflow as tf
import datetime, os
from tensorflow.keras.layers import *
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from PIL import Image

import cv2

app = Sanic("AI")

@app.post("/get_mask")
async def callModel(request):
    
    model=tf.keras.models.load_model("./saved_model/model")

    def create_mask(pred_mask: tf.Tensor) -> tf.Tensor:
        pred_mask = tf.argmax(pred_mask, axis=-1)
        pred_mask = tf.expand_dims(pred_mask, axis=-1)
        return pred_mask

    newtime = datetime.datetime.now()
    time = newtime.strftime("%d%m%Y%H%M%S")
    content = request.json
    email = content["email"]
    upload_file_path = content["upload_file_path"]
    saved_cloth_path = content["saved_cloth_path"]
    print(f"../{upload_file_path}")
    img = tf.keras.utils.load_img(f"../{upload_file_path}", target_size=(128, 128))
    img_array = tf.keras.utils.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0) # Create a batch
    predictions = model.predict(img_array)
    pred_mask = create_mask(predictions)

    tf.keras.utils.save_img(f"./mask/{email}{time}.png",pred_mask[0])

    mask = cv2.imread(f'./mask/{email}{time}.png')
    image = cv2.imread(f'../{upload_file_path}')
    # cv2.imwrite(f"./saved_pic/{email}{time}.jpg",image)

    mask = cv2.cvtColor(mask,cv2.COLOR_BGR2GRAY)
    mask = cv2.resize(mask,(image.shape[1],image.shape[0]))
    masked = cv2.bitwise_and(image,image,mask = mask)
    masked = cv2.GaussianBlur(masked,(5,5),20,0,cv2.BORDER_DEFAULT)
    savePath = f'../{saved_cloth_path}/After-{email}-{time}.png'
    afterFileName = f'After-{email}-{time}.png'
    cv2.imwrite(savePath, masked)
    return json({'Path': savePath, 'afterFileName': afterFileName})



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)


