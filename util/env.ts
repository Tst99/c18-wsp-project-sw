import dotenv from "dotenv";

dotenv.config();


const env = {
    DB_NAME: process.env.DB_NAME || "project3",
    DB_USER: process.env.DB_USERNAME || "postgres",
    DB_PASSWORD: process.env.DB_PASSWORD || "postgres",
    TEST_DB_NAME: process.env.DB_NAME || "project3-test",
    TEST_DB_USER: process.env.DB_USERNAME || "postgres",
    TEST_DB_PASSWORD: process.env.DB_PASSWORD || "postgres",
    SERVER_PORT: process.env.SERVER_PORT || 8081,
    ORIGIN:process.env.ORIGIN || "http://localhost:8081",
    GOOGLE_CLIENT_ID: process.env.GOOGLE_CLIENT_ID,
    GOOGLE_CLIENT_SECRET: process.env.GOOGLE_CLIENT_SECRET,
    SECRET: process.env.SECRET || "project",
    NODE_ENV: "development",
};

export default env;