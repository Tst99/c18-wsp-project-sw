import { Application } from "express";
import ClothesController from "./controller/clothes_controller";
import UserController from "./controller/user_controller";

export interface RouterOptions {
    app: Application;
    userController: UserController;
    clothesController :ClothesController
}

type Season = 'summer' | 'winter'| 'autumn' | 'spring'
type MainType = 'bottom' | 'top' | 'full'
export interface clothesStyleOption {
    color?: string[],
    season?: Season[],
    brand?: string[],
    type?:  string,
    main_type?: MainType[],
    like?: boolean
}
export interface UserInfo{
    id: number;
    username: string;
    password: string;
    email: string;
    icon: string;
    role: string;
    created_at: Date;
    updated_at: Date;
}

export interface Msg{
    msg: string
}

export interface UploadFile{
    afterFileName?: string;
    afterFileRoute?: string;
    beforeFileName?: string;
    beforeFileRoute?: string
}