yarn init -y
yarn knex migrate:latest
yarn knex seed:run
# server DB command
sudo su postgres
psql
> CREATE DATABASE wall;
> CREATE USER wall WITH PASSWORD 'some very strong password' SUPERUSER;
> ALTER ROLE wall WITH LOGIN;
> quit

nano .env
yarn install
yarn knex migrate:latest
yarn knex seed:run # Normally you only need to run this for the first time.
forever start index.js