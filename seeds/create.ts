import { Knex } from "knex";
import { hashPassword } from '../util/hash'
// import randomColor  from "randomcolor";
// const randomColorRGBA = ()=>{
//     return randomColor({
//         format: 'rgba',
//         alpha: 0.5 // e.g. 'rgba(9, 1, 107, 0.5)',
//      })
// }




export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("user_emotion").del();
    await knex("clothes").del();
    await knex("emotion_reference").del();
    await knex("information").del();
    await knex("users").del();

    // Inserts seed entries
    const userIdList = await knex("users").insert([
        { username: 'user1', password: await hashPassword('user1'), email: 'user1@gmail.com', icon: 'defaultIcon.jpg', role: 'user' },
        { username: 'user2', password: await hashPassword('user2'), email: 'user2@gmail.com', icon: 'defaultIcon.jpg', role: 'user' },
        { username: 'user3', password: await hashPassword('user3'), email: 'user3@gmail.com', icon: 'defaultIcon.jpg', role: 'user' },
        { username: 'admin1', password: await hashPassword('admin1'), email: 'admin1@gmail.com', icon: 'defaultIcon.jpg', role: 'admin' },
        { username: 'admin2', password: await hashPassword('admin2'), email: 'admin2@gmail.com', icon: 'defaultIcon.jpg', role: 'admin' },
        { username: 'admin3', password: await hashPassword('admin3'), email: 'admin3@gmail.com', icon: 'defaultIcon.jpg', role: 'admin' },
    ]).returning('id');


    await knex('information').insert([
        { content: 'I am admin1', url: "https://www.youtube.com/embed/hmWlXEqs_pY", user_id: userIdList[3].id},
        { content: 'I am admin2', url: "https://www.youtube.com/embed/hmWlXEqs_pY", user_id: userIdList[4].id},
        { content: 'I am admin3', url: "https://www.youtube.com/embed/hmWlXEqs_pY", user_id: userIdList[5].id}
    ])
    const clothIdList = await knex('clothes').insert([
        {   user_id: userIdList[1].id,
            original_image: 'before_1.jpg',
            processed_image: 'SmockedPlaysuit1.jpeg',
            color: 'rgba(255,204,213,1)',  //pink color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_5.jpg',
            processed_image: 'SmockedPlaysuit2.jpeg',
            color: 'rgba(255,204,213,1)',  //pink color
            season: 'winter',
            brand: 'I.T',
            main_type: 'bottom',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_6.jpg',
            processed_image: 'SmockedPlaysuit3.jpeg',
            color: 'rgba(51,153,51,1)',  //green color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_7.jpg',
            processed_image: 'SmockedPlaysuit4.jpeg',
            color: 'rgba(51,153,51,1)',  //green color
            season: 'winter',
            brand: 'I.T',
            main_type: 'full',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_2.jpg',
            processed_image: 'SmockedPlaysuit5.jpeg',
            color: 'rgba(1,1,1,1)', //black color
            season: 'winter',
            brand: 'I.T',
            main_type: 'bottom',
        },

        {   user_id: userIdList[2].id,
            original_image: 'before_3.jpg',
            processed_image: 'SmockedPlaysuit6.jpeg',
            color: 'rgba(1,1,1,1)', //black color
            season: 'summer',
            brand: 'I.T',
            main_type: 'full',
        },
        {   user_id: userIdList[3].id,
            original_image: 'before_4.jpg',
            processed_image: 'SmockedPlaysuit7.jpeg',
            color: `rgba(51,51,255,1)`, //blue color
            season: 'summer',
            brand: 'Levi\'s',
            main_type: 'bottom',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_6.jpg',
            processed_image: 'hmgoepprod1.jpeg',
            color: 'rgba(255,51,51,1)',  //red color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_6.jpg',
            processed_image: 'hmgoepprod2.jpeg',
            color: 'rgba(255,192,76,1)',  //orange color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_6.jpg',
            processed_image: 'hmgoepprod3.jpeg',
            color: 'rgba(247,247,227,1)',  //beige color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_6.jpg',
            processed_image: 'hmgoepprod4.jpeg',
            color: 'rgba(255,255,51,1)',  //yellow color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_6.jpg',
            processed_image: 'hmgoepprod5.jpeg',
            color: 'rgba(235,183,229,1)',  //purple color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_6.jpg',
            processed_image: 'hmgoepprod6.jpeg',
            color: 'rgba(93,73,69,1)',  //brown color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_6.jpg',
            processed_image: 'SmockedPlaysuit7.jpeg',
            color: 'rgba(182,182,182,1)',  //grey color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_6.jpg',
            processed_image: 'SmockedPlaysuit8.jpeg',
            color: 'rgba(255,255,255,1)',  //white color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },
        {   user_id: userIdList[1].id,
            original_image: 'before_6.jpg',
            processed_image: 'SmockedPlaysuit9.jpeg',
            color: 'rgba(228,186,66,1)',  //bronze color
            season: 'winter',
            brand: 'I.T',
            main_type: 'top',
        },

    ]).returning('id')
    
    const emotionIdList = await knex('emotion_reference').insert([
        {emotion: 'like'},
        {emotion: 'dislike'},
        {emotion: 'collection'}
    ]).returning('id')
    console.log(emotionIdList);
    
    await knex('user_emotion').insert([
        {   user_id: userIdList[1].id,
            cloth_up_id: clothIdList[0].id,
            cloth_lower_id: clothIdList[1].id,
            emotion_id: emotionIdList[2].id
        },
        {   user_id: userIdList[1].id,
            cloth_up_id: clothIdList[2].id,
            cloth_lower_id: clothIdList[1].id,
            emotion_id: emotionIdList[2].id
        },
        {   user_id: userIdList[1].id,
            cloth_up_id: clothIdList[2].id,
            cloth_lower_id: null,
            emotion_id: emotionIdList[0].id
        }
        
    ])
};
