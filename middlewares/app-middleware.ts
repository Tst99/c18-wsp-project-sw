import grant from "grant";
import env from "../util/env";
import expressSession from "express-session";
import express from "express";
import { logger } from "../util/logger";


export const grantExpress = grant.express({
    "defaults":{
        "origin": env.ORIGIN,
        "transport": "session",
        "state": true,
    },
    "google":{
        "key": env.GOOGLE_CLIENT_ID || "",
        "secret": env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["profile","email"],
        "callback": "/login/google"
    }
});

export const expressSessionGate = expressSession({
    secret:env.SECRET,
    resave:true,
    saveUninitialized:true,
});

export function requestLogger(req: express.Request, res: express.Response, next: express.NextFunction) {
    logger.debug(`${req.method}-${req.path}`);
    next();
}

export function handle404(req: express.Request, res: express.Response) {
    logger.error(`Requesting : ${req.method}-${req.path}  but got 404`);
    res.redirect("/404.html");
}

export const firstLandingPage = (req:express.Request, res: express.Response)=>{
    res.redirect('/login.html');
}