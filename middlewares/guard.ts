import express from "express";
import { createResultJSON } from "../util/response-helper";

export const isLoggedIn = (req: express.Request, res: express.Response, next: express.NextFunction) => {

    
    if (req.session && req.session["user"]) {
        //called Next here
        next();
    } else {
        // redirect to index page
        res.redirect("/");
        // res.status(401).json(createResultJSON(null, { message: "Not Authorized" }));
    }
};
