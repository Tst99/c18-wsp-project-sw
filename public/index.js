require("ts-node/register");
require("../main")

// api url
const api_url =
    "https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=rhrread&lang=en";

// Defining async function
async function getapi(url) {

    // Storing response
    const response = await fetch(url);

    // Storing data in form of JSON
    const data = await response.json();
    console.log(data['warningMessage']);
    if (response) {
        hideloader();
    }
    show(data['temperature']['data']);
}

// Calling that async function
getapi(api_url);

// Function to hide the loader
function hideloader() {
    document.getElementById('loading').style.display = 'none';
}

// Function to define innerHTML for HTML table
function show(data) {
    console.log(data)
    let tab =
        `<tr>
            <th>Place</th>  
            <th>Temperature</th>  
		</tr>`;

    // Loop to access all rows
    for (let r of data) {
        tab += `<tr>
<td>${r.place}</td>
<td>${r.value}℃</td>

</tr>`;
    }
    // Setting innerHTML as tab variable
    document.querySelector("#weather").innerHTML = tab;
}



