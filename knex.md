# move
yarn knex migrate:latest
yarn knex migrate:up
yarn knex migrate:down
yarn knex migrate:rollback

# create
yarn knex migrate:make create
yarn knex seed:make -x ts create
yarn knex seed:run

# install
yarn add knex @types/knex pg @types/pg
yarn knex init -x ts

~~~js
import dotenv from 'dotenv';
dotenv.config();

module.exports = {
  development: {
    client: 'postgresql',
    connection: {
      database: process.env.DB_NAME,
      user:     process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    } 
  },

  staging: {
    ...
  },

  production: {
    ...
  }
};
~~~