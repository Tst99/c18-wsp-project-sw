let userInfoForm = document.querySelector("#user-info");
let changeIconFrom = document.querySelector('#change_icon')

async function getUser() {
    let res = await fetch("/profile");
    let user = await res.json();
    //Username
    let usernameLabel = document.querySelector("#username_display");
    usernameLabel.innerText = user.username;
    // User icon
    let userIconLabel = document.querySelector("#user_icon");
    userIconLabel.innerHTML = `<img src=/${user.icon ?? "/assets/images/defaultIcon.jpg"} alt="user icon">`;
    // // Go to other page
    // let toWardrobeLabel = document.querySelector('#btn-my_wardrobe')
    // toWardrobeLabel.addEventListener("click", async function () {
    //     let res = await fetch("/wardrobe.html");
    //     await res.json();
    //     if (res.ok) {
    //         window.location.href = "/wardrobe.html";
    //     } else {
    //         window.location.href = "./login/login.html";
    //     }
    // });
}

getUser();

// Change user icon
changeIconFrom.addEventListener('submit',async (e)=>{
    e.preventDefault();
    let iconObj = {
        icon: e.value
    };

    const form = e.target;
    const formData = new FormData(form);
    // formData.append('newIcon', form.newIcon.files[0]);
    let res = await fetch("/user/icon", {
        method: "PUT",
        body: formData,
    });

    if (!res.ok) {
        console.log("Update fail");
        return;
    }
    console.log("update ok");
    window.location.reload("/profile.html");
})

// Change username and password
userInfoForm.addEventListener("submit", async (e) => {
    // Step 1 : Preparation
    e.preventDefault();
    let infoObj = {
        // username: userInfoForm.newUsername.value,
        username: userInfoForm.newUsername.value,
        password: userInfoForm.newPassword.value,
    };

    // Step 2 : sending request to server
    let res = await fetch("/user", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(infoObj),
    });

    if (!res.ok) {
        console.log("Update fail");
        return;
    }
    console.log("update ok");
    window.location.reload("/profile.html");
});
