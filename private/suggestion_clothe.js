// api url
const api_url =
    "https://data.weather.gov.hk/weatherAPI/opendata/weather.php?dataType=rhrread&lang=en";
let temperature
// Defining async function
async function getapi(url) {

    // Storing response
    const response = await fetch(url);

    // Storing data in form of JSON
    const data = await response.json();
    console.log(data['warningMessage']);
    if (response) {
        hideloader();
    }
    show(data['temperature']['data']);

    // find temperature
    temperature = data['temperature']['data'][0].value
    console.log("king's Park temperature = " + temperature)
    document.getElementById("temperature").value = `${temperature}`
}

// Calling that async function
getapi(api_url);

// Function to hide the loader
function hideloader() {
    document.getElementById('loading').style.display = 'none';
}

// Function to define innerHTML for HTML table
function show(data) {
    console.log(data)
    let tab =
        `<tr>
            <th>Place</th>  
            <th>Temperature</th>  
		</tr>`;

    // Loop to access all rows
    for (let r of data) {
        tab += `<tr>
<td>${r.place}</td>
<td>${r.value}℃</td>

</tr>`;
    }
    // Setting innerHTML as tab variable
    document.querySelector("#weather").innerHTML = tab;
}
// document.querySelector("#ootd_1_upper").innerHTML = ;
let btn = document.querySelector('form#submit_form')
btn.addEventListener('submit', async (event) => {
    event.preventDefault()
    let content = btn.content.value
    if (isNaN(content) || content == "") {
        document.getElementById("error_message").hidden = false
        return
    } else {
        document.getElementById("error_message").hidden = true
        document.getElementById("limit_error").hidden = true
    }
    let res = await fetch('/get_ootd', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ content })
    })
    let result = await res.json()
    console.log(result)
    console.log(result.type);
    if (result.type == "limited_clothes") {
        document.getElementById("limit_error").hidden = false
    }else {
        document.getElementById("limit_error").hidden = true
        document.getElementById("error_message").hidden = true
    }
    if (result.length >= 2) {
        if (result[0].type == "full") {
            document.querySelector("#ootd_1_upper").innerHTML = `<img src="${result[0].path1}">`
            document.querySelector("#ootd_1_lower").innerHTML = ""
        } else {
            document.querySelector("#ootd_1_upper").innerHTML = `<img src="${result[0].path1}">`
            document.querySelector("#ootd_1_lower").innerHTML = `<img src="${result[0].path2}">`
        }
        if (result[1].type == "full") {
            document.querySelector("#ootd_2_upper").innerHTML = `<img src="${result[1].path1}">`
            document.querySelector("#ootd_2_lower").innerHTML = ""
        } else {
            document.querySelector("#ootd_2_upper").innerHTML = `<img src="${result[1].path1}">`
            document.querySelector("#ootd_2_lower").innerHTML = `<img src="${result[1].path2}">`
        }
    }
})
