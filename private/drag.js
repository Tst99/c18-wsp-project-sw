// const element1 = document.querySelector('.element1')
// const element2 = document.querySelector('.element2')
// const container = document.querySelector('.container')

export default function dragElementInContainer (element1, container){
    let newPosX = 0, newPosY = 0, startPosX = 0, startPosY = 0;
    element1.onmousedown = (e) => {
        e.preventDefault()
        startPosX = e.clientX;
        startPosY = e.clientY;

        document.addEventListener('mousemove', mouseMove);
        document.addEventListener('mouseup', function () {
            document.removeEventListener('mousemove', mouseMove);
        });
    }
    function mouseMove(e) {
        // calculate the new position
        newPosX = startPosX - e.clientX;
        newPosY = startPosY - e.clientY;
        // with each move we also want to update the start X and Y
        startPosX = e.clientX;
        startPosY = e.clientY;
        // console.log(element1);
        element1.style.top = (element1.offsetTop - newPosY) + "px";
        element1.style.left = (element1.offsetLeft - newPosX) + "px";
        let currentRelativeY = getCurrentRelativeY(element1), currentRelativeX = getCurrentRelativeX(element1)

        if (currentRelativeY < 0 || currentRelativeY > getUpperLimitY(element1, container)) {
            element1.style.top = (currentRelativeY + newPosY) + "px";
        }
        if (currentRelativeX < 0 || currentRelativeX > getUpperLimitX(element1, container)) {
            element1.style.left = (currentRelativeX + newPosX) + "px";
        }
    }

}
const getCurrentRelativeY = (element) => {
    let currentRelativeY = element.style.top
    currentRelativeY = parseInt(currentRelativeY.substring(0, currentRelativeY.length - 2))
    return currentRelativeY
}
const getCurrentRelativeX = (element) => {
    let currentRelativeX = element.style.left
    currentRelativeX = parseInt(currentRelativeX.substring(0, currentRelativeX.length - 2))
    return currentRelativeX
}

const getUpperLimitY = (element, container) => {
    // console.log(container.clientHeight - element.clientHeight);
    return container.clientHeight - element.clientHeight
}

const getUpperLimitX = (element, container) => {
    return container.clientWidth - element.clientWidth
}

// dragElementInContainer(element1, container)
// dragElementInContainer(element2, container)

