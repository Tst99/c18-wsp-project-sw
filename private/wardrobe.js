const filterBtnList = ['All', 'Color', 'Top', 'Bottom', `Dresses, Jumpsuits, Rompers`, `Brand`, `Season`, `Like`]
const noSubFilterBtnList = ['All', 'Top', 'Bottom', 'Dresses, Jumpsuits, Rompers', 'Like']
const filterContianer = document.querySelector("#select_list")
const selectionDisplay = document.querySelector('.selection')
let styleOption = {}
const addNewBtn = document.querySelector(`#add_new`)
const clothesContainer = document.querySelector(`#container_result`)
const imageTopCloth = document.querySelector('.top_cloth_canvas')
const imageBottomCloth = document.querySelector('.bottom_cloth_canvas')
const imageFullCloth =  document.querySelector('.full_cloth_canvas')
import dragElementInContainer from './drag.js'

async function init() {
    await constructPage()
    addClickStaticElement()
    //fetch all data for the particular user
    await fetchAllClothes(styleOption)
    clothesMatchInit()
}

const addClickStaticElement = () => {
    addNewBtn.onclick = () => {
        //click event --> redirect page(clothes picture upload)
        window.location.href = "/login_success/upload.html"
    }
    const styleContainers = document.querySelectorAll('.style_container')
    for (let styleContainer of styleContainers) {
        let isShow = false
        styleContainer.onclick = (e) => {
            if (e.target.matches('.filter_btn')) {
                const mainFilterBtnContent = e.target.innerText
                if (IsContainSubFilter(mainFilterBtnContent)) {
                    updateStyleOption(mainFilterBtnContent)
                    fetchAllClothes(styleOption)
                } else {
                    //hide OR show the sub filter button
                    isShow = toggleFilterBtnVisble(e, isShow)
                }
            }
            //collect sub filter button selection and fetch data
            if (e.target.parentNode.matches('.styleOptionContainer')) {
                const styleKey = e.target.parentNode.getAttribute('id')
                const styleValue = (styleKey === 'color') ? e.target.getAttribute('id') : e.target.innerHTML
                if (!styleOption[styleKey]) styleOption[styleKey] = []

                if (!styleOption[styleKey].includes(styleValue)) {
                    styleOption[styleKey].push(styleValue)
                } else {
                    const index = styleOption[styleKey].indexOf(styleValue)
                    styleOption[styleKey].splice(index, 1)
                }
                fetchAllClothes(styleOption)
            }
        }
    }
}


async function likeClothes(e, clothes_id, main_type) {
    const RouteSelection = lowerCaseFirstLetter(e.target.innerText) //like or unlike
    const res = await fetch(`/${RouteSelection}-clothes/${clothes_id}/${main_type}`, {
        method: 'POST'
    })
    const {msg} = await res.json()
    console.log(msg)
    if (res.status === 200) {
        console.log(e.target.innerText)
        e.target.innerText = (e.target.innerText === 'Liked') ? 'Unliked' : 'Liked'
    }
}

async function deleteClothes(clothes_id, main_type) {
    let res = await fetch(`/clothes/${clothes_id}/${main_type}`, {
        method: 'delete'
    })
    const {msg} = await res.json()
    if (res.status === 200) {
        init()
    }
}

const editClothes = (imageRoute) => {
    window.location.href = `/login_success/information_clothes.html?route=${imageRoute}`
}

const addClickEmotionBtn = () => {
    const emotionBtnContainer = document.querySelector('#container_result')
    emotionBtnContainer.onclick = async (e) => {
        const emotionSubContainer = (e.target.matches('i'))? e.target.parentNode.parentNode.parentNode : e.target.parentNode.parentNode
        const clothesImg = emotionSubContainer.querySelector('img')
        const clothes_id = emotionSubContainer.getAttribute('clothes_id');
        const main_type = emotionSubContainer.getAttribute('main_type');
        const imageRoute = clothesImg.getAttribute('src')
        if (e.target.matches('#btn_like')) {
            return await likeClothes(e, clothes_id, main_type);
        }

        if (e.target.matches('#btn_delete')) {
            return  await deleteClothes(clothes_id, main_type);
        }

        if (e.target.matches('#btn_edit')) {
            return await editClothes(imageRoute);
        }

    }
}

function buildClothInfoCards(result, likeButtonList) {
    return result.clothesResult.reduce((html, clothesInfo) => {
        const {processed_image, clothes_id, main_type} = clothesInfo
        const likeButtonContent = (likeButtonList.indexOf(clothes_id) === -1) ? 'Liked' : 'Unliked'
        return html + `<div class="link cards clothes-photo-result">
                  <div class="filter_sub_container"  clothes_id="${clothes_id}" main_type="${main_type}">
                  <div class="filter_sub_container_img" >
                  <img class="image clothes-photo" src="${processed_image}" alt="">
                  </div>
                    <div id="emotion_btn_container">
                        <button class="ui red basic button btn_emotion" id="btn_like">${likeButtonContent}</button>
                        <button class="ui red basic button btn_emotion" id="btn_delete"><i class="trash alternate icon"></i>Delete</button>
                        <button class="ui red basic button btn_emotion" id="btn_edit"><i class="edit icon"></i>Edit</button>
                    </div>
                    </div>
            </div>`
    }, '');
}

// Button of like & dislike & delete
const fetchAllClothes = async (styleOption) => {
    //fetch clothes data
    const likeButtonList = await fetchLikeButtonInfo()
    const res = await fetch('/clothes', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(styleOption)
    });
    const result = await res.json()
    clothesContainer.innerHTML = buildClothInfoCards(result, likeButtonList)
    updateFilterDisplay()
    addClickEmotionBtn()
    addClickEventImg()
}
const fetchLikeButtonInfo = async () => {
    const res = await fetch('/like_button_info')
    return await res.json()
}
const fetchStyleOption = async () => {
    let res = await fetch('/styleOption')
    res = await res.json()
    return res
}

function buildColorList(styleOptions, filterLabel) {
    return styleOptions[lowerCaseFirstLetter(filterLabel)]?.reduce((html, styleOption) => html +
            `<button ${
                filterLabel === "Color" ?
                    `style="background:${styleOption};" class="btn-color ui label"` : ''
            } 
        id="${styleOption}">
            ${filterLabel === "Color" ?
                '' :
                `${styleOption}`
            }</button>`
        , '');
}

const constructPage = async () => {
    //fetch style info for filter button
    const styleOptions = await fetchStyleOption()
    //create style option button
    filterContianer.innerHTML = ""
    for (let filterLabel of filterBtnList) {

        //create style tag
        const styleOptionTag = buildColorList(styleOptions, filterLabel)

        //create style button
        if (filterLabel === `Dresses, Jumpsuits, Rompers`) {
            filterContianer.innerHTML += `
            <div class="style_container">
            <button class="full_filter_btn filter_btn btn-outline-danger">${filterLabel}</button>
            <div class="styleOptionContainer hide" id="full">
                ${styleOptionTag}
            </div>
            </div>`
            continue
        }
        filterContianer.innerHTML += `
        <div class="style_container">
            <button class="${lowerCaseFirstLetter(filterLabel)}_filter_btn filter_btn btn-outline-danger">${filterLabel}</button>
            <div class="styleOptionContainer hide " id=${lowerCaseFirstLetter(filterLabel)}>
                ${styleOptionTag}
            </div>
        </div>`
    }
}
const lowerCaseFirstLetter = (str) => {
    return str.charAt(0).toLowerCase() + str.slice(1)
}
const IsContainSubFilter = (btnContent) => {
    return (noSubFilterBtnList.indexOf(btnContent) !== -1)
}
const toggleFilterBtnVisble = (e, isShow) => {
    e.currentTarget.querySelector('div')
        .classList
        .replace(isShow ? 'hide' : 'show', isShow ? 'show' : 'hide')
    return !isShow
}
const updateStyleOption = (mainFilterBtnContent) => {
    mainFilterBtnContent = (mainFilterBtnContent === 'Dresses, Jumpsuits, Rompers') ? "full" : mainFilterBtnContent
    mainFilterBtnContent = lowerCaseFirstLetter(mainFilterBtnContent)
    let index;
    switch (mainFilterBtnContent) {
        case 'all':
            styleOption = {}
            break;
        case 'top':
        case 'bottom':
        case 'full':
            if (!styleOption['main_type']) styleOption['main_type'] = [];
            index = styleOption['main_type'].indexOf(mainFilterBtnContent)
            index === -1 ? styleOption['main_type'].push(mainFilterBtnContent) : styleOption['main_type'].splice(index, 1)
            break;
        case 'like':
            //like-to-do
            styleOption['like'] = (styleOption['like']) ? !styleOption['like'] : true
            break;
        default:
            break;
    }

}
const updateFilterDisplay = () => {
    selectionDisplay.innerHTML = ""
    for (let key in styleOption) {
        const styleList = styleOption[key]
        selectionDisplay.innerHTML += key + ': '
        if (key === 'like') continue
        for (let style of styleList) {
            selectionDisplay.innerHTML += style + ', '
        }
        selectionDisplay.innerHTML += '<br>'
    }
    // selectionDisplay.innerText += styleOption
}

init()


const clothesMatchInit = ()=>{
    const addNewMatchBtn = document.querySelector('#add_match')   
    const matchingButton = document.querySelector(`#nav-btn-matching`)
    const canvasContainer = document.querySelector('.matching_canvas_container')
    const clothesResultContainer = document.querySelector('.container_result_imgandbtn')
    let isShow = true
    // Show hide matching canvas
    addNewMatchBtn.onclick = ()=>{
        if (isShow) {
            clothesResultContainer.classList.add('col-7')
            canvasContainer.classList.remove('hide')
            canvasContainer.classList.add('col-5', 'show')
            hideElement(imageTopCloth)
            hideElement(imageBottomCloth)
            hideElement(imageFullCloth)  
            dragElementInContainer(imageTopCloth, canvasContainer)
            dragElementInContainer(imageBottomCloth, canvasContainer)
            dragElementInContainer(imageFullCloth, canvasContainer)
        } else {
            clothesResultContainer.classList.remove('col-7')
            canvasContainer.classList.add('hide')
            canvasContainer.classList.remove('col-5', 'show')
        }
        isShow = !isShow 
    }
}

const placeImagetoCanvas = (e)=>{
    const imageRoute = e.target.getAttribute('src')
    const imageType = e.target.parentNode.parentNode.getAttribute('main_type')

    console.log(imageType);
    if(imageType == 'top') {
        imageTopCloth.src = imageRoute
        showElement(imageTopCloth)
    }
    if(imageType == 'bottom'){
        imageBottomCloth.src = imageRoute
        showElement(imageBottomCloth)
    } 
    if(imageType == 'full') {
        imageFullCloth.src = imageRoute
        showElement(imageFullCloth)
    }
}

const showElement = (element)=>{
    element.classList.add('show')
    element.classList.remove('hide')
}
const hideElement = (element)=>{
    element.classList.remove('show')
    element.classList.add('hide')
}

const addClickEventImg = ()=>{
    const clothImages = document.querySelectorAll('.clothes-photo')
    clothImages.forEach((clothImage)=>{
        clothImage.onclick = placeImagetoCanvas
    })
}

