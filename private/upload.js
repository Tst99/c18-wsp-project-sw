const fileUpload = document.querySelector('#file_upload')
const previewUploadContainer = document.querySelector('.before_image_container')
const beforeForm = document.querySelector(`#upload_container`)
const afterImageContainer = document.querySelector(`.after_image_container`)
const confirmBtn = document.querySelector(`.confirm_btn`)
const processBtn = document.querySelector(`.process_btn`)

let imageUpload; // upload image 
fileUpload.onchange = async (e)=>{
    let previewURL;
    if(!e.target.files.length){
        resetImage()
        return 
    }
    if(e.target.files[0].type.includes('heic')){   
        const formData = new FormData()
        formData.append('filename', e.target.files[0])
        resetImage()
        const res = await fetch('/HEICImage', {
            method: 'POST',
            body: formData
        })
        imageUpload = await res.blob()
    }else{
        imageUpload = e.target.files[0]
    }
    previewURL = URL.createObjectURL(imageUpload)
    previewUploadContainer.innerHTML = `<img class="upload_image" src="${previewURL}" alt="">`
}


beforeForm.addEventListener('submit', async (e)=>{
    e.preventDefault()
    const res = await uploadForm()
    const {msg} = await res.json()
    alert(msg)
    if(msg == 'No files is found'){
        return 
    }
    fetchProcessedImage()
})

processBtn.addEventListener('click', async()=>{
    afterImageContainer.innerHTML = ``
    await fetchProcessedImage()
})

confirmBtn.addEventListener('click', (e)=>{
    //To-do
    e.preventDefault()
    if(!afterImageContainer.innerHTML){
        alert('Please upload picture first')
        return
    }
    window.location.href = "/login_success/information_clothes.html";
})





const resetImage = ()=>{
    previewUploadContainer.innerHTML = ``
    afterImageContainer.innerHTML = ``
    imageUpload = ``
}
const uploadForm = async()=>{
    if(!beforeForm.filename.files.length){
        alert('empty image is uploaded')
        return
    }
    if(!imageUpload){
        alert('please upload a image')
        return 
    }
    const formData = new FormData()
    formData.append('filename', imageUpload)

    const res = await fetch('/before-clothes-image', {
        method: 'POST',
        body: formData
    })
    return res
}
const fetchProcessedImage = async()=>{
    const result = await fetch('/processdImage')
    const image = await result.blob()
    const aferImagePreview = URL.createObjectURL(image)
    afterImageContainer.innerHTML = `<img class="after_image" src="${aferImagePreview}" alt="">`
} 