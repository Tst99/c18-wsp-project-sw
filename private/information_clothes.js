
//edit color code
const row1ColorList = ['rgba(255,204,213,1)', 'rgba(255,51,51,1)', 'rgba(255,192,76,1)', 'rgba(247,247,227,1)', 'rgba(255,255,51,1)', 'rgba(51,153,51,1)', 'rgba(192, 34, 161, 1)']
const row2ColorList = ['rgba(51,51,255,1)', 'rgba(235,183,229,1)', 'rgba(93,73,69,1)', 'rgba(182,182,182,1)', 'rgba(255,255,255,1)', 'rgba(1,1,1,1)', 'rgba(228,186,66,1)']
const colorRowOne = document.querySelector('.color_row1')
const colorRowTwo = document.querySelector('.color_row2')
const colorRowsContainer = document.querySelector('#colour_select_container')
const photo_knockout = document.querySelector('#photo_knockout')
const form = document.querySelector('#container_info_change')
const saveButton = document.querySelector('#btn_save_change')
let lastActiveButton = null;
let colorSelect;

const init = async()=>{
    buildColorRow()
    getClotheImage()
    colletColorData()
    postData()
}



const buildColorRow = ()=>{
    row1ColorList.forEach((RgbaColor)=>{
        colorRowOne.innerHTML +=`<button style='background-color: ${RgbaColor};' color = "${RgbaColor}"></button>`
    })
    row2ColorList.forEach((RgbaColor)=>{
        colorRowTwo.innerHTML +=`<button style='background-color: ${RgbaColor};' color = "${RgbaColor}"></button>`
    })
}
const getEditRoute = ()=>{
    const address = window.location.search
    parameterList = new URLSearchParams(address)
    return parameterList.get('route')
}
const getClotheImage = async ()=>{
    if(getEditRoute()){
        photo_knockout.src = getEditRoute()
        return
    }
    const res = await fetch('/clothImage')
    const {msg, error} = await res.json()
    if(error){
        alert(error)
        window.location.href = "/login_success/upload.html";
        return 
    }
    photo_knockout.src = msg
}
const colletColorData = ()=>{
    colorRowsContainer.addEventListener('click', getColorSelection)
}
const getColorSelection = (e)=>{
    e.preventDefault()
    if(e.target.matches('button')){
        colorSelect = e.target.getAttribute('color');
        e.target.classList.add("active")
        if(lastActiveButton){
            lastActiveButton.classList.remove("active")
        } 
        lastActiveButton = e.target
    }
}
const createFromObj = ()=>{
    const formData = new FormData();
    formData.append('main_type', form.type_select.value)
    formData.append('brand', form.brand.value)
    formData.append('season', form.season.value)
    formData.append('color', colorSelect)
    getEditRoute()? formData.append('editRoute', getEditRoute()) : formData.append('editRoute', null)
    return formData
}
const isDataMissing = ()=>{
    if(!form.type_select.value || !form.brand.value|| !form.season.value||!colorSelect){
        return true
    }
    return false
} 
const formSubmit = async(e)=>{
    e.preventDefault()
    //check if data is missing
    if(isDataMissing()){
        alert('Some data is missing!')
        return
    }
    const res = await fetch('/new-clothe', {
        method:"POST",
        body: createFromObj()
    })
    const {msg, error} = await res.json()
    console.log('res.', error);
    if(error){
        alert('upload fail')
        return
    }
    alert(msg)
    window.location.href = "/login_success/wardrobe.html"
}

const postData = ()=>{
    saveButton.addEventListener('click', formSubmit)
}

init()