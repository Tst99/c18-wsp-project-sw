import multer from "multer";
import path from "path";

const heicImageStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve( __dirname,"../private/uploads/HEIC"));
    },
    filename: function (req, file, cb) {
        let user = req.session["user"];
        const {email} = user
        cb(null, `${email}-${Date.now()}-${file.originalname.split(".")[0]}.${file.mimetype.split('/')[1]}`);
    },
});
export const heicImageUpload = multer({ storage: heicImageStorage });

const storageBefore = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve(__dirname, '../private/uploads/beforeClothes'))
    },
    filename: function (req, file, cb) {
        let user = req.session["user"];
        const {email} = user
        cb(null, `Before-${email}-${Date.now()}-${file.originalname.split(".")[0]}.jpg`);
    }
  })
  
export const uploadBefore = multer({ storage: storageBefore })

