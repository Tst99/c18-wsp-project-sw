import multer from "multer";
import path from "path";

const userIconStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve( __dirname,"../public/user_icon"));
    },
    filename: function (req, file, cb) {
        let user = req.session["user"];
        const {email} = user
        cb(null, `${email}-${Date.now()}-${file.originalname.split(".")[0]}.png`);
    },
});
export const userIconUpload = multer({ storage:userIconStorage });